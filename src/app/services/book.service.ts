import { Injectable } from '@angular/core';
import { Book } from '../models/bookModel';

const books: Book[] = [
  {
    "id": 1,
    "serialNumber": 123,
    "name": "Three Hearts and Three Lions",
    "authors": "Poul Anderson",
    "bookQty": 20,
    "bookQtyCopy": 20
  },
  {
    "id": 2,
    "serialNumber": 223,
    "name": "Alice's Adventures in Wonderland",
    "authors": "Lewis Carroll",
    "bookQty": 20,
    "bookQtyCopy": 20
  },
  {
    "id": 3,
    "serialNumber": 222,
    "name": "The Lion, the Witch and the Wardrobe",
    "authors": "C.S. Lewis",
    "bookQty": 20,
    "bookQtyCopy": 20
  },
  {
    "id": 4,
    "serialNumber": 234,
    "name": "The Lord of the Rings Trilogy",
    "authors": "J.R.R. Tolkien",
    "bookQty": 20,
    "bookQtyCopy": 20
  }
];

 // const booksString = localStorage.setItem("booksString", JSON.stringify(books));

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor() { 
    const bookCase = localStorage.getItem("booksString");
    if(!bookCase) {
      localStorage.setItem("booksString", JSON.stringify(books));
    }
  }

  getBooks() {
    let retrievedData = localStorage.getItem("booksString");
    let books2 = JSON.parse(retrievedData);
    return books2;
  }

  getOneBook(id: number) {
    let currentBooksState = this.getBooks();
    let book: Book;
    currentBooksState.forEach(elem => {
      elem.id === id ?  book = elem : "";
    })
  return book;
  }

  addBook(book: Book){
    let currentBooksState = this.getBooks();
    currentBooksState.push(book);
    localStorage.setItem("booksString", JSON.stringify(currentBooksState));
  }

  deleteBook(id: number) {
    console.log(id);
    let currentBooksState = this.getBooks();
    let newBooksState = currentBooksState.filter(item => item.id != id);
    localStorage.setItem("booksString", JSON.stringify(newBooksState));
  }

  updateBook(book: Book){
    let currentBooksState = this.getBooks();
    let updateBooksState = currentBooksState.map(elem => {
    if (elem.id === book.id) {
      elem = book;
    }
    return elem;
    });
    localStorage.setItem("booksString", JSON.stringify(updateBooksState));
  }
}
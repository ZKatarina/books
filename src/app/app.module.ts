import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BooksListComponent } from './pages/books-list/books-list.component';
import { HeaderComponent } from './core/header/header.component';
import { NavbarComponent } from './core/navbar/navbar.component';
import { MatTableModule } from '@angular/material/table'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateBookComponent } from './pages/update-book/update-book.component';
import { TableComponent } from './pages/table/table.component';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalComponent } from './share/modal/modal.component'; 
import { MatDialogModule } from '@angular/material/dialog';
import { AddBookComponent } from './pages/add-book/add-book.component';


@NgModule({
  declarations: [
    AppComponent,
    BooksListComponent,
    AddBookComponent,
    HeaderComponent,
    NavbarComponent,
    UpdateBookComponent,
    TableComponent,
    ModalComponent,
  ],
  
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatTableModule,
    MatSortModule,
    FormsModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    MatDialogModule
  ],

  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ModalComponent],
  
})
export class AppModule { }

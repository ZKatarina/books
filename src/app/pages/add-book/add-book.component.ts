import { Component, OnInit } from '@angular/core';
import { Book } from 'src/app/models/bookModel';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BookService } from 'src/app/services/book.service';
import { Router } from '@angular/router';

@Component({
  selector: 'katarina-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss']
})
export class AddBookComponent implements OnInit {
  newBook: Book;
  formBook: FormGroup;
  books=[];

  constructor(private fb: FormBuilder, private bookService: BookService, private router :Router) {
    this.createForm();
   }

  ngOnInit() {
    this.books = this.bookService.getBooks();
  }

  createForm(){
    this.formBook = this.fb.group({
      id: [""],
      name: ["", Validators.required],
      serialNumber: ["", Validators.required],
      authors: ["", Validators.required],
      bookQty: ["", Validators.required],
      bookQtyCopy: ["", Validators.required]
    });
  }

  lastId() {
    let lastId: number = 0;
    for (let book of this.books) {
      let id = book.id;

      if (id > lastId){
        lastId = id;
      }
    }
    return lastId;
  }
  
  onSubmit(){
    let id = this.lastId() + 1;
    this.formBook.controls['id'].setValue(id);
    this.bookService.addBook(this.formBook.value);
    this.router.navigate(['books']);
  }
}

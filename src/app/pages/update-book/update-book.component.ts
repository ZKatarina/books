import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { BookService } from 'src/app/services/book.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Book } from 'src/app/models/bookModel';

@Component({
  selector: 'katarina-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.scss']
})
export class UpdateBookComponent implements OnInit {
  updateBook: Book;
  formBook: FormGroup;
  books = [];

  constructor(private fb: FormBuilder, private bookService: BookService, private router :Router, private route: ActivatedRoute) {
    this.createForm();
   }

  ngOnInit() {
    this.books = this.bookService.getBooks();
    let id: string = this.route.snapshot.params.id;
    this.updateBook = this.bookService.getOneBook(Number(id));
    console.log(this.updateBook);
    this.formBook.patchValue(this.updateBook); 
    console.log(this.formBook);
  }

  createForm(){
    this.formBook = this.fb.group({
      id: [""],
      name: ["", Validators.required],
      serialNumber: ["", Validators.required],
      authors: ["", Validators.required],
      bookQty: ["", Validators.required],
      bookQtyCopy: ["", Validators.required]
    });
  }

  lastId() {
    let lastId: number = 0;
    for (let book of this.books) {
      let id = book.id;
      id > lastId ? lastId = id : "";
    }
    return lastId;
  }
  
  onSubmit(){ 
    this.bookService.updateBook(this.formBook.value);
    this.router.navigate(['books']);
  }
}

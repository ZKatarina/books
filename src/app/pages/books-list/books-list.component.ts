import { Component, OnInit, ViewChild } from '@angular/core';
import { BookService } from 'src/app/services/book.service';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Book } from 'src/app/models/bookModel';
import { MatPaginator } from '@angular/material/paginator';



@Component({
  selector: 'katarina-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.scss']
})


export class BooksListComponent implements OnInit {
  books = [];
  
  constructor( private bookService: BookService) { }

  ngOnInit() {
    this.books = this.bookService.getBooks();
  }

}

import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { BookService } from 'src/app/services/book.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Book } from 'src/app/models/bookModel';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material/dialog';
import { ModalComponent } from 'src/app/share/modal/modal.component';


@Component({
  selector: 'katarina-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() books: Book[];
  displayedColumns: string[] = ['id', 'serialNumber', 'name', 'authors', 'bookQty', 'bookQtyCopy', 'delete', 'update'];
  dataSource: any = [];
  

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
 
  
  constructor( private bookService: BookService, public dialog: MatDialog) {}

  ngOnInit() {
    this.refreshList();
  }

  refreshList(){
    this.dataSource = new MatTableDataSource(this.bookService.getBooks());
    console.log(this.books);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  onDelete(id: number): void {
    this.bookService.deleteBook(id);
    this.refreshList();
    console.log(this.books);
  }

  openDialog(book: Book): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '450px',
      data: book
    });
  }
}
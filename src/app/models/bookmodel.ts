export interface Book {
	id: number;
	serialNumber: number;
	name: string;
	authors: string;
	bookQty: number;
	bookQtyCopy:number;
}
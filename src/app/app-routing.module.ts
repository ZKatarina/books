import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksListComponent } from './pages/books-list/books-list.component';
import { AddBookComponent } from './pages/add-book/add-book.component';
import { UpdateBookComponent } from './pages/update-book/update-book.component';


const routes: Routes = [
{path: "books", component: BooksListComponent},
{path: "books/add", component: AddBookComponent},
{path: "books/:id", component: UpdateBookComponent},
{path: "", redirectTo: "books", pathMatch:"full"},

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

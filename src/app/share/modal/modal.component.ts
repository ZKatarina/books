import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'katarina-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  object: any;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
   
    this.object = Object.entries(data);
  }

  ngOnInit() {
    
  }
}
